package com.muhardin.endy.training.iso8583.atm;

import javax.print.attribute.standard.MediaSize;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.Socket;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class BalanceInquiryDemo {
    public static void main(String[] args) throws Exception {
        System.out.println("ATM Simulator");

        Map<Integer, String> balanceInquiryRequest = new HashMap<>();
        balanceInquiryRequest.put(2, "161234567800001111");
        balanceInquiryRequest.put(3, "100001"); // Balance Inquiry
        balanceInquiryRequest.put(11, "123321");
        balanceInquiryRequest.put(12, "134600");
        balanceInquiryRequest.put(13, "20200819");
        balanceInquiryRequest.put(32, "08ENDYBANK");
        balanceInquiryRequest.put(37, "123456789012");
        balanceInquiryRequest.put(41, "ATM-01  ");
        //balanceInquiryRequest.put(65, "001");
        //balanceInquiryRequest.put(128, "001");
        balanceInquiryRequest.put(70, "001");
        balanceInquiryRequest.put(49, "0360");
        //balanceInquiryRequest.put(52, "1234");

        String isomsg = ISO8583Helper.formatMessageForSending("0100", balanceInquiryRequest);
        System.out.println("Message to be sent : " + isomsg);

        int port = 54321;
        Socket socketConnection = new Socket("localhost", port);
        PrintWriter output = new PrintWriter(socketConnection.getOutputStream());

        output.print(isomsg);
        output.flush();

        BufferedReader reader = new BufferedReader(new InputStreamReader(socketConnection.getInputStream()));
        char[] byteLength = new char[4];
        reader.read(byteLength);
        String strLength = new String(byteLength);
        System.out.println("Response length : " + strLength);
        char[] responseData = new char[Integer.parseInt(strLength)];
        reader.read(responseData);
        System.out.println("Response content : "+new String(responseData));

        reader.close();
        output.close();
        socketConnection.close();
    }
}

package com.muhardin.endy.training.iso8583.atm;

import java.math.BigInteger;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

public class ISO8583Helper {
    public static String computeBitmap(Map<Integer, String> isoMsgData) {
        BigInteger primaryBitmap = BigInteger.ZERO;
        BigInteger secondaryBitmap = BigInteger.ZERO;
        /*
        primaryBitmap = primaryBitmap.setBit(64-2);
        primaryBitmap = primaryBitmap.setBit(64-3);
        primaryBitmap = primaryBitmap.setBit(64-11);
        primaryBitmap = primaryBitmap.setBit(64-12);
        primaryBitmap = primaryBitmap.setBit(64-13);
         */

        // check usage of DE > 64
        Integer maxSlot = Collections.max(isoMsgData.keySet());
        System.out.println("Max DE : "+maxSlot);

        if (maxSlot > 64) {
            primaryBitmap = primaryBitmap.setBit(64 - 1);
        }

        for (Integer de : isoMsgData.keySet()) {
            if(de > 64) {
                secondaryBitmap = secondaryBitmap.setBit(128 - de);
            } else {
                primaryBitmap = primaryBitmap.setBit(64 - de);
            }
        }

        if (maxSlot > 64) {
            return formatBitmap(primaryBitmap) + formatBitmap(secondaryBitmap);
        }

        return formatBitmap(primaryBitmap);
    }

    public static String calculateMessageLength(String isomsg) {
        return String.format("%1$4s", isomsg.length())
                .replace(' ', '0');
    }

    public static String formatBitmap(BigInteger bitmap) {
        return String.format("%1$16s", bitmap.toString(16))
                .replace(' ', '0');
    }

    public static String padLeft(String input, char pad, Integer length){
        return String.format("%1$"+length+"s", input)
                .replace(' ', pad);
    }


    public static String formatMessageForSending(String mti,
                                                 Map<Integer, String> isomessage) {
        String strIso = mti
                + ISO8583Helper.computeBitmap(isomessage)
                + isomessage
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .map(Map.Entry::getValue)
                .collect(Collectors.joining());
        return calculateMessageLength(strIso) + strIso;
    }
}
